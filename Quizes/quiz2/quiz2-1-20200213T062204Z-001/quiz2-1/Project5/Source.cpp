#include <iostream>

using namespace std;
int main()
{
	float area;
	float circumference;
	float pi = 3.14;
	int radius;

	cout << "enter radius: ";
	cin >> radius;

	area = pi * (radius ^ 2);
	circumference = 2 * pi * radius;

	cout << "area = " << area << endl;
	cout << "circumference = " << circumference << endl;
	system("pause");
	return 0;
}