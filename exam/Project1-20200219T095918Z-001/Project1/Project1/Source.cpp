#include<iostream>

using namespace std;

int main()
{
	int minDamage, maxDamage;
	int enemyMinDamage, enemyMaxDamage;
	int player, enemy;
	int damage, enemyDamage;
	char duel = 'w';


	// initialization of the player and the enemy
	cout << "input the player's health: ";
	cin >> player;
	cout << "inpute the player's minimum damage: ";
	cin >> minDamage;
	cout << "input the player's maximum damage(must be equal or higher than the player's minimum damage): ";
	cin >> maxDamage;
	cout << "input the enemy's health: ";
	cin >> enemy;
	cout << "input the enemy's minimum damage: ";
	cin >> enemyMinDamage;
	cout << "input the enemy's maximum damage: ";
	cin >> enemyMaxDamage;

	// the duel 
	while (player > 0 && enemy > 0) {
		cout << "player health: " << player << endl;
		cout << "enemy health: " << enemy << endl;
		cout << "select you action:" << endl;
		cout << "==================" << endl;
		cout << "[A] = Attack" << endl;
		cout << "[D] = Defend" << endl;
		cout << "[W] = Wild Attack" << endl;
		cin >> duel;
		damage = rand() % (minDamage - maxDamage) + maxDamage;
		enemyDamage = rand() % (enemyMinDamage - enemyMaxDamage + 1) + enemyMaxDamage;
		int enemyAction = 1 + (rand() % 3);
		switch (duel) {
		case 'a':
		case 'A':
			if (enemyAction = 1) {
				cout << "enemy takes " << damage << " damage" << endl;
				enemy = enemy - damage;
				cout << "player takes " << enemyDamage << endl;
				player = player - enemyDamage;
			}
			else if (enemyAction = 2) {
				cout << "enemy takes " << damage << " damage" << endl;
				enemy = enemy - damage;
			}
			else {
				cout << "enemy takes " << damage << " damage" << endl;
				enemy = enemy - damage;
				cout << "player takes " << enemyDamage * 2 << " damage" << endl;
				player = player - enemyDamage * 2;
			}
			break;
		case 'd':
		case 'D':
			if (enemyAction = 3) {
				cout << "Player: too slow" << endl;
				cout << "enemy takes" << damage << " damage" << endl;
				enemy = enemy - damage;
			}
			else if (enemyAction = 2) {
				cout << "both combatants only took a defensive stance with no effect" << endl;
			}
			else {
				cout << "player takes" << enemyDamage / 2 << " damage" << endl;
				player = player - enemyDamage;
			}
			break;
		case 'w':
		case 'W':
			if (enemyAction = 1) {
				cout << "enemy takes " << damage * 2 << " damage" << endl;
				damage = damage * 2;
				enemy = enemy - damage;
				cout << "player takes " << enemyDamage << " damage" << endl;
				player = player - enemyDamage;
			}
			else if (enemyAction = 2) {
				cout << "player takes " << enemyDamage << " damage" << endl;
				player = player - enemyDamage;
			}
			else {
				cout << "enemy takes " << damage * 2 << " damage" << endl;
				enemy = enemy - damage * 2;
				cout << "player takes " << damage * 2 << " damage" << endl;
				player = player - enemyDamage * 2;
			}
			break;
		default:
			cout << "no action available";
			break;
		}
		system("pause");
	}
	if (enemy <= 0) {
		cout << "player wins the duel" << endl;
	}
	else if (player <= 0) {
		cout << "enemy wins, game over" << endl;
	}
	else {
		cout << "error" << endl;
	}
	system("pause");
	return 0;
}